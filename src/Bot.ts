require('dotenv').config()
import { Client } from "discord.js";
import interactionCreate from "./listeners/interactionCreate";
import ready from "./listeners/ready";

console.log("Bot is starting... ",process.env.token);

const client = new Client({
    intents: []
});


ready(client);
interactionCreate(client);
client.login(process.env.token as string)
